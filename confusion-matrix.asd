(defsystem "confusion-matrix"
  :description "Library for creating a confusion matrix, incrementally adding information, and
retrieving statistical information."
  :version "0.1"
  :author "Peter Lane <peterlane@gmx.com>"
  :maintainer "Peter Lane <peterlane@gmx.com>"
  :homepage "https://peterlane.codeberg.pages/confusion-matrix-lisp/"
  :licence "MIT"
  :components ((:file "src/package")
               (:file "src/confusion-matrix" :depends-on ("src/package")))
  :depends-on ("alexandria")
  :in-order-to ((test-op (test-op "confusion-matrix/tests"))))

(defsystem "confusion-matrix/tests"
  :components ((:file "src/tests"))
  :depends-on ("confusion-matrix")
  :perform (test-op (o c) (symbol-call :confusion-matrix :run-tests)))
 
