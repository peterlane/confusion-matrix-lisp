# confusion-matrix

Library for creating a confusion matrix, incrementally adding information, and
retrieving statistical information.

# Overview

A confusion matrix represents "the relative frequencies with which each of a
number of stimuli is mistaken for each of the others by a person in a task
requiring recognition or identification of stimuli" (R. Colman, _A Dictionary
of Psychology_, 2008).  Each row represents the _predicted_ label of an
instance, and each column represents the _observed_ label of that instance.
Numbers at each (row, column) reflect the total number of instances of
predicted label "row" which were observed as having label "column".

A two-class example is:

    Observed        Observed      | 
    Positive        Negative      | Predicted
    ------------------------------+------------
        a               b         | Positive
        c               d         | Negative

Here the value:

* a - is the number of true positives (those predicted positive and observed positive)
* b - is the number of false negatives (those predicted positive but observed negative)
* c - is the number of false positives (those predicted negative but observed positive)
* d - is the number of true negatives (those predicted negative and observed negative)

From this table we can calculate statistics like:

* true positive rate - a/(a+b)
* positive recall - a/(a+c)

As statistics can also be calculated for the negative label, e.g. the true
negative rate is d/(c+d), the functions below have an optional `:label-for`
key, to specify which label they are calculated for: the default is to report
for the first label named when the matrix is created

The implementation supports confusion matrices with more than two labels. When
more than two labels are in use, the statistics are calculated as if the first,
or named, label were positive and all the other labels are grouped as if
negative.

# Usage

The following example creates a simple two-label confusion matrix, prints a few
statistics and displays the table - notice that the package "confusion-matrix"
has the alias "cm".

```
(require 'asdf)
(require 'confusion-matrix)

(defvar cm (cm:make-confusion-matrix :labels '(:pos :neg)))

(cm:confusion-matrix-add cm :pos :pos 10)
(cm:confusion-matrix-add cm :pos :neg 3)
(cm:confusion-matrix-add cm :neg :neg 20)
(cm:confusion-matrix-add cm :neg :pos 5)

(format t "Confusion Matrix~%~%~a~%" cm)
(format t "Precision: ~f~&" (cm:precision cm))
(format t "Recall: ~f~&" (cm:recall cm))
(format t "MCC: ~f~&" (cm:matthews-correlation cm))
```

which outputs:

    > sbcl --script example-confusion-matrix.lisp
    Confusion Matrix
    
    Observed |
    POS NEG  | Predicted
    ---------+----------
    10   3   | POS
     5  20   | NEG
    
    Precision: 0.6666667
    Recall: 0.7692308
    MCC: 0.55248505

# MIT License

Copyright (c) 2021, Peter Lane <peterlane@gmx.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

