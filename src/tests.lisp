(in-package :confusion-matrix)

(defun check (query error-message)
  (if query
    (format t ".")
    (format t "~&Error: ~a~&" error-message)))

(defun eq-nums-p (num-1 num-2 &optional (tolerance 0.001))
  "Check if two floats are within tolerance"
  (< (abs (- num-1 num-2)) tolerance))

(defun test-empty-case ()
  (let ((cm (make-confusion-matrix))) ; use default classes
    (check (zerop (confusion-matrix-total cm)) "empty: total")
    (check (zerop (confusion-matrix-count cm :positive :positive)) "empty: pos/pos count")
    (check (zerop (confusion-matrix-count cm :positive :negative)) "empty: pos/neg count")
    (check (zerop (confusion-matrix-count cm :negative :negative)) "empty: neg/neg count")
    (check (zerop (confusion-matrix-count cm :negative :positive)) "empty: neg/pos count")
  ))

(defun test-two-classes-1 ()
  (let ((cm (make-confusion-matrix))) ; use default classes
    (dotimes (i 10) (confusion-matrix-add cm :positive :positive)) ; try with default
    (confusion-matrix-add cm :positive :negative 5)
    (dotimes (i 20) (confusion-matrix-add cm :negative :negative)) ; try with default
    (confusion-matrix-add cm :negative :positive 5)

    (check (equalp '(:positive :negative) (confusion-matrix-labels cm)) "two: labels")
    (check (= 10 (confusion-matrix-count cm :positive :positive)) "two: pos/pos count")
    (check (= 5 (confusion-matrix-count cm :positive :negative)) "two: pos/neg count")
    (check (= 20 (confusion-matrix-count cm :negative :negative)) "two: neg/neg count")
    (check (= 5 (confusion-matrix-count cm :negative :positive)) "two: neg/pos count")

    (check (= 40 (confusion-matrix-total cm)) "two: total")
    (check (= 10 (true-positive cm)) "two: true-positive :positive")
    (check (= 5 (false-negative cm)) "two: false-negative :positive")
    (check (= 20 (true-negative cm)) "two: true-negative :positive")
    (check (= 5 (false-positive cm)) "two: false-positive :positive")
    (check (= 20 (true-positive cm :for-label :negative)) "two: true-positive :negative")
    (check (= 5 (false-negative cm :for-label :negative)) "two: false-negative :negative")
    (check (= 10 (true-negative cm :for-label :negative)) "two: true-negative :negative")
    (check (= 5 (false-positive cm :for-label :negative)) "two: false-positive :negative")

    (check (eq-nums-p 0.6667 (true-rate cm)) "two: true-rate :positive")
    (check (eq-nums-p 0.8 (true-rate cm :for-label :negative)) "two: two-rate :negative")
    (check (eq-nums-p 0.2 (false-rate cm)) "two: false-rate :positive")
    (check (eq-nums-p 0.3333 (false-rate cm :for-label :negative)) "two: false-rate :negative")
    (check (eq-nums-p 0.6667 (precision cm)) "two: precision :positive")
    (check (eq-nums-p 0.8 (precision cm :for-label :negative)) "two: precision :negative")
    (check (eq-nums-p 0.6667 (recall cm)) "two: recall :positive")
    (check (eq-nums-p 0.8 (recall cm :for-label :negative)) "two: recall :negative")
    (check (eq-nums-p 0.6667 (sensitivity cm)) "two: sensitivity :positive")
    (check (eq-nums-p 0.8 (sensitivity cm :for-label :negative)) "two: sensitivity :negative")
    (check (eq-nums-p 0.75 (overall-accuracy cm)) "two: overall accuracy")
    (check (eq-nums-p 0.6667 (f-measure cm)) "two: f-measure :positive")
    (check (eq-nums-p 0.8 (f-measure cm :for-label :negative)) "two: f-measure :negative")
    (check (eq-nums-p 0.7303 (geometric-mean cm)) "two: geometric-mean")
    ))

  ;; Example from: 
  ;; https://www.datatechnotes.com/2019/02/accuracy-metrics-in-classification.html
(defun test-two-classes-2 ()
  (let ((cm (make-confusion-matrix))) ; use default classes
    (confusion-matrix-add cm :positive :positive 5)
    (confusion-matrix-add cm :positive :negative 1)
    (confusion-matrix-add cm :negative :negative 3)
    (confusion-matrix-add cm :negative :positive 2)

    (check (= 11 (confusion-matrix-total cm)) "two2: total")
    (check (= 5 (true-positive cm)) "two2: true-positive")
    (check (= 1 (false-negative cm)) "two2: false-negative")
    (check (= 2 (false-positive cm)) "two2: false-positive")
    (check (= 3 (true-negative cm)) "two2: true-negative")

    (check (eq-nums-p 0.7142 (precision cm)) "two2: precision")
    (check (eq-nums-p 0.8333 (recall cm)) "two2: recall")
    (check (eq-nums-p 0.7272 (overall-accuracy cm)) "two2: overall-accuracy")
    (check (eq-nums-p 0.7692 (f-measure cm)) "two2: f-measure")
    (check (eq-nums-p 0.8333 (sensitivity cm)) "two2: sensitivity")
    (check (eq-nums-p 0.6 (specificity cm)) "two2: specificity")
    (check (eq-nums-p 0.4407 (cohen-kappa cm)) "two2: cohen-kappa")
    (check (eq-nums-p 0.5454 (prevalence cm)) "two2: prevalence")
    ))

  ;; Examples from:
  ;; https://standardwisdom.com/softwarejournal/2011/12/matthews-correlation-coefficient-how-well-does-it-do/
(defun test-two-classes-3 ()
  (flet ((test-case (a b c d e f g h i)
                    (let ((cm (make-confusion-matrix)))
                      (confusion-matrix-add cm :positive :positive a)
                      (confusion-matrix-add cm :positive :negative b)
                      (confusion-matrix-add cm :negative :negative c)
                      (confusion-matrix-add cm :negative :positive d)

                      (check (eq-nums-p e (matthews-correlation cm)) "two3: matthews-correlation")
                      (check (eq-nums-p f (precision cm)) "two3: precision")
                      (check (eq-nums-p g (recall cm)) "two3: recall")
                      (check (eq-nums-p h (f-measure cm)) "two3: f-measure")
                      (check (eq-nums-p i (cohen-kappa cm)) "two3: cohen-kappa"))))

    (test-case 100 0 900 0 1.0 1.0 1.0 1.0 1.0)
    (test-case 65 35 825 75 0.490 0.4643 0.65 0.542 0.4811)
    (test-case 50 50 700 200 0.192 0.2 0.5 0.286 0.1666)))

(defun test-three-classes ()
  (let ((cm (make-confusion-matrix :labels '(:red :blue :green))))
    (confusion-matrix-add cm :red :red 10)
    (confusion-matrix-add cm :red :blue 7)
    (confusion-matrix-add cm :red :green 5)
    (confusion-matrix-add cm :blue :red 20)
    (confusion-matrix-add cm :blue :blue 5)
    (confusion-matrix-add cm :blue :green 15)
    (confusion-matrix-add cm :green :red 30)
    (confusion-matrix-add cm :green :blue 12)
    (confusion-matrix-add cm :green :green 8)

    (check (= 112 (confusion-matrix-total cm)) "three: total")
    (check (= 10 (true-positive cm)) "three: true-positive :red")
    (check (= 12 (false-negative cm)) "three: false-negative :red")
    (check (= 50 (false-positive cm)) "three: false-positive :red")
    (check (= 13 (true-negative cm)) "three: true-negative :red")
    (check (= 5 (true-positive cm :for-label :blue)) "three: true-positive :blue")
    (check (= 35 (false-negative cm :for-label :blue)) "three: false-negative :blue")
    (check (= 19 (false-positive cm :for-label :blue)) "three: false-positive :blue")
    (check (= 18 (true-negative cm :for-label :blue)) "three: true-negative :blue")
    (check (= 8 (true-positive cm :for-label :green)) "three: true-positive :green")
    (check (= 42 (false-negative cm :for-label :green)) "three: false-negative :green")
    (check (= 20 (false-positive cm :for-label :green)) "three: false-positive :green")
    (check (= 15 (true-negative cm :for-label :green)) "three: true-negative :green")
    ))

(defun run-tests ()
  (format t "~&Testing: ") 
  (test-empty-case)
  (test-two-classes-1)
  (test-two-classes-2)
  (test-two-classes-3)
  (test-three-classes)
  (format t "~%-------- Done~&"))

