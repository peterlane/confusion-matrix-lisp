(defpackage :confusion-matrix
  (:use :cl)
  (:nicknames :cm)
  (:export
    :make-confusion-matrix
    :confusion-matrix-p
    :confusion-matrix-add
    :confusion-matrix-count
    :confusion-matrix-total
    :false-negative
    :false-positive
    :false-rate
    :f-measure
    :geometric-mean
    :cohen-kappa
    :matthews-correlation
    :overall-accuracy
    :precision
    :prevalence
    :recall
    :sensitivity
    :specificity
    :true-negative
    :true-positive
    :true-rate
    )
  (:documentation "Lisp library for creating a confusion matrix, incrementally adding information, and retrieving statistical information.

[horizontal]
documentation:: https://peterlane.codeberg.page/confusion-matrix-lisp/[]
repository:: https://codeberg.org/peterlane/confusion-matrix-lisp/[]"))

