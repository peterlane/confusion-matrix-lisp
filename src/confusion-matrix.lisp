(in-package :confusion-matrix)

;; ---------------------------------------------------------------------------
;; Confusion Matrix definition and functions

(defstruct (confusion-matrix 
             (:constructor make-confusion-matrix 
                           (&key (labels '(:positive :negative)) 
                                 (counts (mapcar #'(lambda (pair) (cons pair 0))
                                                 (alexandria:map-product 'list labels labels)))))
             (:print-function print-confusion-matrix))
  labels counts)

(defun print-confusion-matrix (cm str depth)
  "Prints the confusion matrix to given stream"
  (declare (ignore depth))
  (let ((title-line "Observed ")
        (label-line ""))
    (dolist (label (confusion-matrix-labels cm))
      (setf label-line (uiop:strcat label-line (string label) " ")))

    (let ((longest (max (length title-line) (length label-line))))
      (format str "~va|~%~va| Predicted~%" longest title-line longest label-line)
      (format str "~a+----------~%" (make-string longest :initial-element #\-)))

    (dolist (predicted (confusion-matrix-labels cm))
      (let ((count-line ""))
        (dolist (observed (confusion-matrix-labels cm))
          (setf count-line 
                (uiop:strcat count-line
                             (format nil "~v:@<~a~> " (length (string observed)) (confusion-matrix-count cm predicted observed)))))
        (format str "~va| ~a~%" (length title-line) count-line predicted)))))

(defun confusion-matrix-add (cm predicted observed &optional (total 1))
  "Adds total to the count for given (predicted observed) labels, errors if labels not known"
  (incf (cdr (confusion-matrix-retrieve cm predicted observed)) total))

(defun confusion-matrix-count (cm predicted observed)
  "Returns the count for given (predicted observed) labels, errors if labels not known"
  (cdr (confusion-matrix-retrieve cm predicted observed)))

(defun confusion-matrix-retrieve (cm predicted observed)
  "Internal function, returns the pair for given (predicted observed) labels, errors if labels not known"
  (alexandria:if-let ((pair (assoc (list predicted observed) (confusion-matrix-counts cm) :test #'equalp)))
                     pair
                     (error "Unknown predicted/observed pair in confusion matrix")))

(defun confusion-matrix-total (cm)
  "Returns the total number of instances referenced in the confusion matrix"
  (reduce #'+ (confusion-matrix-counts cm) :key #'cdr :initial-value 0))

(defun false-negative (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Returns the number of instances of the given label which are incorrectly observed"
  (let ((total 0))
    (dolist (observed (confusion-matrix-labels cm))
      (unless (eql for-label observed)
        (incf total (cdr (confusion-matrix-retrieve cm for-label observed)))))
    total))

(defun false-positive (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Returns the number of instances incorrectly observed of the given label"
  (let ((total 0))
    (dolist (predicted (confusion-matrix-labels cm))
      (unless (eql predicted for-label)
        (incf total (cdr (confusion-matrix-retrieve cm predicted for-label)))))
    total))

(defun false-rate (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Proportion of instances of label incorrectly observed out of all instances not originally of that label"
  (let ((fp (false-positive cm :for-label for-label))
        (tn (true-negative cm :for-label for-label)))
    (divide fp (+ fp tn))))

(defun f-measure (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Harmonic mean of the precision and recall for the given label"
  (let ((precision (precision cm :for-label for-label))
        (recall (recall cm :for-label for-label)))
    (divide (* 2 precision recall)
            (+ precision recall))))

(defun geometric-mean (cm)
  "nth root of product of true-rate for each label"
  (let ((product 1.0))
    (dolist (label (confusion-matrix-labels cm))
      (setf product (* product (true-rate cm :for-label label))))
    (expt product (/ 1 (length (confusion-matrix-labels cm))))))

(defun cohen-kappa (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Cohen's Kappa statistic compares observed accuracy with an expected accuracy"
  (let* ((tp (true-positive cm :for-label for-label))
         (fn (false-negative cm :for-label for-label))
         (fp (false-positive cm :for-label for-label))
         (tn (true-negative cm :for-label for-label))
         (total (+ tp fn fp tn))
         (total-accuracy (divide (+ tp tn) total))
         (random-accuracy (divide (+ (* (+ tn fp) (+ tn fn))
                                     (* (+ fn tp) (+ fp tp)))
                                  (* total total))))
    (divide (- total-accuracy random-accuracy)
            (- 1 random-accuracy))))

(defun matthews-correlation (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Measure of the quality of binary classifications"
  (let ((tp (true-positive cm :for-label for-label))
        (fn (false-negative cm :for-label for-label))
        (fp (false-positive cm :for-label for-label))
        (tn (true-negative cm :for-label for-label)))
    (divide (- (* tp tn) (* fp fn))
            (sqrt (* (+ tp fp) (+ tp fn) (+ tn fp) (+ tn fn))))))

(defun overall-accuracy (cm)
  "Proportion of instances which are correctly labelled"
  (divide (reduce #'+ 
                  (mapcar #'(lambda (label) (confusion-matrix-count cm label label))
                          (confusion-matrix-labels cm)))
          (confusion-matrix-total cm)))

(defun precision (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Proportion of instances of given label which are correct"
  (let ((tp (true-positive cm :for-label for-label))
        (fp (false-positive cm :for-label for-label)))
    (divide tp (+ tp fp))))

(defun prevalence (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Proportion of instances observed of given label, out of total"
  (let ((tp (true-positive cm :for-label for-label))
        (fn (false-negative cm :for-label for-label)))
    (divide (+ tp fn) (confusion-matrix-total cm))))

(defun recall (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Recall is equal to the true rate, for a given label"
  (true-rate cm :for-label for-label))

(defun sensitivity (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Sensitivity is another name for the true positve rate (recall), for a given label"
  (true-rate cm :for-label for-label))

(defun specificity (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Specificity is 1 - false-rate, for a given label"
  (- 1 (false-rate cm :for-label for-label)))

(defun true-negative (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Returns the number of instances NOT of the given label which are correctly observed"
  (let ((total 0))
    (dolist (predicted (confusion-matrix-labels cm))
      (unless (eql for-label predicted)
        (incf total (cdr (confusion-matrix-retrieve cm predicted predicted)))))
    total))

(defun true-positive (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Returns the number of instances of the given label correctly observed"
  (cdr (confusion-matrix-retrieve cm for-label for-label)))

(defun true-rate (cm &key (for-label (first (confusion-matrix-labels cm))))
  "Proportion of instances of given label which are correctly observed"
  (let ((tp (true-positive cm :for-label for-label))
        (fn (false-negative cm :for-label for-label)))
    (divide tp (+ tp fn))))

(defun divide (x y)
  "safe-divide: returns 0 if y is zero"
  (if (zerop y)
    y
    (/ x y)))

